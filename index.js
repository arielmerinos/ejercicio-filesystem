//Imports de los módulos
//fs y path
const fs = require("fs/promises");
const path = require('path');
const USERS_PATH = path.resolve('users.json');
const getUsers = async() => {
    //Leer el archivo
    try{
        const users = await fs.readFile(USERS_PATH, 'utf-8');
        console.log(users);
        return JSON.parse(users);
    }catch(error){
        console.log(error)
    }
    //Regresar el arreglo de usuarios como objeto literal Javascript (sin notación JSON)
};

const addUser = async (userObj) => {
    //leer el archivo 
    //convertir el contenido en formato JSON en un objeto JS
    try{
        const users = await getUsers();
        users.push(userObj);
        await fs.writeFile(USERS_PATH, JSON.stringify(users))
        return userObj;
    } catch (error){
        return error;
    }
    //sobreescribir el arreglo en el archivo
    //si el proceso se realizó correctamente tendrás que regresar el objeto de usuario
    //que acabas de agregar en el arreglo
};

const clearUsers = async () => {
    //Vaciar el arreglo y sobreescribir el archivo
    try{
        await fs.writeFile(USERS_PATH, JSON.stringify([]));
        return true;
    } catch (error){
        return error;
    }
    //Si el proceso se realizó correctamente tendrás que regresar true
}

module.exports = {
    getUsers,
    addUser,
    clearUsers,
};
